# clojure-packages-demo
---

* https://en.wikipedia.org/wiki/Clojure
* https://en.wikipedia.org/wiki/Software_transactional_memory
* https://www.google.com/search?q=what+is+clojure+good+for
* Clojure is especially suited to concurrency tasks
  * https://stackoverflow.com/questions/4260522/what-is-clojure-useful-for

## Packages Lists
* [clojars.org](https://clojars.org/):
  [clojure](https://www.google.com/search?q=clojure+site%3Aclojars.org)
  @ [Google](https://www.google.com/search?q=site%3Aclojars.org)
* [most popular clojure libraries](https://www.google.com/search?q=most+popular+clojure+libraries)
* [*The Clojure Toolbox*](https://www.clojure-toolbox.com/) (clj, cljs)
* [*Awesome Clojure*](https://github.com/razum2um/awesome-clojure) ...2019...
* [*What are the most used Clojure libraries?*
  ](https://jakemccrary.com/blog/2017/04/17/what-are-the-most-used-clojure-libraries/).
  2017 Jake McCrary
* [*The Top 100 Clojure Libraries in 2016 - After Analyzing 30,000+ Dependencies*
  ](https://blog.overops.com/the-top-100-clojure-libraries-in-2016-after-analyzing-30000-dependencies/)
  2016 Alex Zhitnitsky
* [*Clojure Libraries*](https://clojure.org/community/libraries) @ clojure.org
* [*Clojure Core and Contrib API Overview*](http://clojure.github.io/) @ clojure.github.io
* [*A Directory of Clojure Libraries*](http://clojure-doc.org/articles/ecosystem/libraries_directory.html) @ clojure-doc.org
* [packages.debian.org/sid/java](https://packages.debian.org/sid/java/)
  @ [Google
  ](https://www.google.com/search?q=clojure+site%3Ahttps%3A%2F%2Fpackages.debian.org%2Fsid%2Fjava%2F)

## Other Demo
* https://gitlab.com/hub-docker-com-demo/clojure (maybe some info shoud be taken from where?)
* https://gitlab.com/clojure-packages-demo/ferret (maybe some info shoud be taken from where?)
* https://gitlab.com/debian-packages-demo/clojure

### [Tail call](https://en.wikipedia.org/wiki/Tail_call)
* [(recur expr*)](https://clojure.org/reference/special_forms#recur)

## Books
* [*Programming Clojure*](https://www.worldcat.org/title/programming-clojure) 2018 3rd ed.
* [*Web development with Clojure : build bulletproof web apps with less code*](https://www.worldcat.org/title/web-development-with-clojure-build-bulletproof-web-apps-with-less-code) 2016 2nd ed.
  * Safari Business and Technical Books Combo Collection
* [*Clojure recipes*](https://www.worldcat.org/title/clojure-recipes) 2016
  * Safari Business and Technical Books Combo Collection
* [*Clojure applied : from practice to practitioner*](https://www.worldcat.org/title/clojure-applied-from-practice-to-practitioner)
  * Safari Business and Technical Books Combo Collection
* Living Clojure : [an introduction and training plan for developers]
  * https://www.worldcat.org/title/living-clojure-an-introduction-and-training-plan-for-developers
  * Safari Business and Technical Books Combo Collection
* https://www.worldcat.org/title/clojure-cookbook-recipes-for-functional-programming
  * Safari Business and Technical Books Combo Collection

## Talks
* [The Best Clojure Tech Talks Ever, Ranked By Wilson Score](https://techyaks.com/clojure-all-ldconf.html)

## Clojure's Object-Oriented Features
* [clojure object programming](https://www.google.com/search?q=clojure+object+programming)
* [*When Should You Use Clojure's Object-Oriented Features?*](http://thinkrelevance.com/blog/2013/11/07/when-should-you-use-clojures-object-oriented-features)
* [*Reinventing OOP with Clojure*](https://nvbn.github.io/2015/01/30/reinventing-oop-with-clojure/)
* +[*Object-oriented Clojure*](https://dzone.com/articles/object-oriented-clojure) 2012
* [closure defrecord](https://www.google.com/search?q=closure+defrecord)
* [*Clojure for OOP Folks: How to Design Clojure Programs*](https://speakerdeck.com/stilkov/clojure-for-oop-folks-how-to-design-clojure-programs)
* [*Clojure does Objects Better*](https://gist.github.com/runexec/786b92b97b5a3a0ffac7)

## Implementations
* [*Literature Review*](https://github.com/sventech/ClojureSwift#literature-review)